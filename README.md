# imp

*small demon, a familiar of a witch*

![](imp_color.jpg)

imp is a set of bot creating utilities built on top of [libcwtch-rs](https://git.openprivacy.ca/cwtch.im/libcwtch-rs)

It is in the very early prototype stage with one prototype use in the Cwtch [update bot](https://git.openprivacy.ca/dan/update_bot/)

## Usage

Start with creating a `Behaviour` struct and populating it with your desired set of bot behaviours, then `imp::spawn` your bot with the behaviour.

To handle Cwtch events you can either 
- Define a struct fulfilling the `imp::EventHandler::event_loop` function which has the capacity to support all the events libCwtch can emit
- Override specific `on_x_event` functions in `imp::EventHandler` such as `on_new_message_from_contact`
  - This is newer and more will be defined in later versions 

Finally, run the imp `my_imp.event_loop::<MyEventHandlerType>(update_bot.borrow_mut());`

## Examples

- Private [rust-bert](https://github.com/guillaume-be/rust-bert) chat bot - [rust-bert-bot](https://git.openprivacy.ca/dan/rust-bert-bot)
- Public utility bot - [Cwtch Update Bot](https://git.openprivacy.ca/dan/update_bot/)

